> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


## Zackary Hall

### Assignment 5 Requirements:

*Four Parts:*

1. Introduction_to_R_Setup_and_Tutorial
2. lis4369_a5.R Coding from Demos
3. Skillsets 13-15
4. Python Questions Ch. 11 and 12


#### README.md file should include the following items:

* Screenshots of r_tutorial
* Screenshots of lis4369_a5
* Screenshots of Skillsets


#### Assignment Screenshots:
r_tutorial:                                      |  
:-----------------------------------------------:|
![r_tutorial](img/rtut.PNG)                      |  

r_tutorial_plot_1:                               |  r_tutorial_plot_2:                             |
:-----------------------------------------------:|:-----------------------------------------------:|
![r_tutorial_plot_1](img/tutplot1.PNG)           |  ![r_tutorial_plot_2](img/tutplot2.PNG)         |  

lis4369_a5:                                      |  
:-----------------------------------------------:|
![Jupyter Notebook Screenshot 3](img/a5.PNG)     |

lis4369_a5_plot_1:                               |  lis4369_a5_plot_2:                             |
:-----------------------------------------------:|:-----------------------------------------------:|
![lis4369_a5_plot_1](img/a5hist.PNG)             |  ![lis4369_a5_plot_2](img/a5boxplot.PNG)            |

Skillset 13 Sphere Volume:                       |  
:-----------------------------------------------:|
![Skillset 13](img/ss13.PNG)                     |  

Skillset 14 Error Handling:                      |
:-----------------------------------------------:|
![Skillset 14](img/ss14.PNG)                     |

Skillset 15 Write/Read File:                     |
:-----------------------------------------------:|
![Skillset 15](img/ss15.PNG)                     |  

#### Other Links:

*Click to return to the main page:*
[Home](https://bitbucket.org/Z_Hall19/lis4369/src/master/)
