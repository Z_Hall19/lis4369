#!/usr/bin/env python3
import functions as f

def main():
    f.get_requirements()
    f.file_write()
    f.file_read()
    f.write_read_file()

if __name__ == "__main__":
    main()
