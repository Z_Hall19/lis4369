> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


## Zackary Hall

### Project 2 Requirements:

*Five Parts:*

1. Backwards-engineer the lis4369_p2_requirements.txt file
2. Save as lis4369_p2.R
3. Two 4-panel RStudio Screenshots executing lis4369_p2.R
4. Minimum of two plots
5. R Questions


#### README.md file should include the following items:

* Link to lis4369_p2.R file: [lis4369_p2.R](p2source/lis4369_p2.R "lis4369_p2.R")
* Screenshots of Output
* Screenshots of at least two plots


#### Assignment Screenshots:
Code Execution:                                  |  
:-----------------------------------------------:|
![execution](img/execute.PNG)                    |  

P2 Output p1:                                    |  
:-----------------------------------------------:|
![p2outputp1](img/p21.PNG)                       |

P2 Output p2:                                    |  P2 Output p3:                                  |
:-----------------------------------------------:|:-----------------------------------------------:|
![p2output2](img/p22.PNG)                        |  ![p2output3](img/p23.PNG)                      |  

P2 Output p4:                                    |  P2 Output p5:                                  |
:-----------------------------------------------:|:-----------------------------------------------:|
![p2output4](img/p24.PNG)                        |  ![p2output5](img/p25.PNG)                      |  

P2 Output p6:                                    |  P2 Output p7:                                  |
:-----------------------------------------------:|:-----------------------------------------------:|
![p2output6](img/p26.PNG)                        |  ![p2output7](img/p27.PNG)                      |  


lis4369_p2 Plot 1:                               |  lis4369_p2 Plot 2:                             |
:-----------------------------------------------:|:-----------------------------------------------:|
![lis4369_p2p1](img/plot_disp_and_mpg_1.png)     |  ![lis4369_p2p2](img/plot_disp_and_mpg_2.png)   |



#### Other Links:

*Click to return to the main page:*
[Home](https://bitbucket.org/Z_Hall19/lis4369/src/master/)
