> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Zackary Hall

### Assignment 2 Requirements:

*Four Parts:*

1. Payroll App
2. Questions for Chapters 3 and 4
3. Bitbucket Repo Link
4. Skillsets


#### README.md file should include the following items:

* Screenshot of a2_payroll_calculator application running with and without overtime
* Link to A2 .ipynb file: [a2_payroll.ipynb](a2_payroll_calculator/a2_payroll.ipynb "A2 Jupyter Notebook")
* Screenshots of Jupyter Notebook
* Screenshots of Skillsets


#### Assignment Screenshots:
Payroll without Overtime:                        |  Payroll with Overtime:
:-----------------------------------------------:|:-----------------------------------------------:
![Payroll calculator w/o overtime](img/not.PNG)  |  ![Payroll calculator w/overtime](img/ot.PNG)

Jupyter Notebook Screenshot 1:                                               |
:--------------------------------------------------------------------------:|
![Jupyter Notebook Screenshot 1](img/a2_payroll_calculator_jupyter_1.png)   |     

Jupyter Notebook Screenshot 2:                    |  Jupyter Notebook Screenshot 3:
:-----------------------------------------------:|:-----------------------------------------------:
![Jupyter Notebook Screenshot 2](img/j2.PNG)     |  ![Jupyter Notebook Screenshot 3](img/j3.PNG)


Skillset 1:                                      |  Skillset 2:                                    |  Skillset 3:
:-----------------------------------------------:|:-----------------------------------------------:|:-----------------------------------------------:
![ss1_square_ft_to_acres](img/ss1.PNG)           |  ![ss2_mile_per_gallon](img/ss2.PNG)            |   ![ss3_it_ict_students_percentage](img/ss3.PNG)



#### Other Links:

*Click to return to the main page:*
[Home](https://bitbucket.org/Z_Hall19/lis4369/src/master/)

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Z_Hall19/bitbucketstationlocations/ "Bitbucket Station Locations")
