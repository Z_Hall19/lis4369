#!/usr/bin/env python3
import functions as f

def main():
    f.get_requirements()

    f.temperature_conversions()

if __name__ == "__main__":
    main()
