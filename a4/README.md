> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


## Zackary Hall

### Project 1 Requirements:

*Four Parts:*

1. Data Analysis 2 App
2. Questions for Chapters 9 and 10
3. Bitbucket Repo Link
4. Skillsets 10, 11, and 12


#### README.md file should include the following items:

* Screenshot of Data Analysis 2 App
    - get_requirements()
    - data_analysis_2()
* Link to a4.ipynb file: [a4_data_anlysis_2.ipynb](a4_data_analysis_2/a4_data_analysis_2.ipynb "A4 Jupyter Notebook")
* Screenshots of Jupyter Notebook
* Screenshots of Skillsets


#### Assignment Screenshots:
Data Analysis 2:                                 |  
:-----------------------------------------------:|
![Data Analysis 2](img/a4.PNG)                   |  

Jupyter Notebook Screenshot 1:                   |  Jupyter Notebook Screenshot 2:                 |  Jupyter Notebook Screenshot 3:
:-----------------------------------------------:|:-----------------------------------------------:|:-----------------------------------------------:
![Jupyter Notebook Screenshot 1](img/jn1.PNG)    |  ![Jupyter Notebook Screenshot 2](img/jn2.PNG)  |  ![Jupyter Notebook Screenshot 2](img/jn3.PNG)

Jupyter Notebook Screenshot 4:                   |  
:-----------------------------------------------:|
![Jupyter Notebook Screenshot 3](img/jn4.PNG)    |  


Skillset 10 Dictionaries:                        |  Skillset 11 Random Number Generator:           |  Skillset 12 Temperature Conversions:
:-----------------------------------------------:|:-----------------------------------------------:|:-----------------------------------------------:
![Skillset 10](img/ss10.PNG)                     |  ![Skillset 11](img/ss11.PNG)                   |  ![Skillset 12](img/ss12.PNG)



#### Other Links:

*Click to return to the main page:*
[Home](https://bitbucket.org/Z_Hall19/lis4369/src/master/)
