> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


## Zackary Hall

### Assignment 3 Requirements:

*Four Parts:*

1. Painting Estimator App
2. Questions for Chapter 6
3. Bitbucket Repo Link
4. Skillsets


#### README.md file should include the following items:

* Screenshot of a3_painting_estimator
    - get_requirements()
    - estimate_painting_cost()
    - print_painting_estimate()
    - print_painting_percentage()
* Link to A3.ipynb file: [a3_painting_estimator.ipynb](a3_painting_estimator/a3_painting_estimator.ipynb "A3 Jupyter Notebook")
* Screenshots of Jupyter Notebook
* Screenshots of Skillsets


#### Assignment Screenshots:
Painting Estimator:                              |  
:-----------------------------------------------:|
![Painting Estimator](img/paint_estimator.PNG)   |  

Jupyter Notebook Screenshot 1:                   |  Jupyter Notebook Screenshot 2:
:-----------------------------------------------:|:-----------------------------------------------:
![Jupyter Notebook Screenshot 1](img/jnb1.PNG)   |  ![Jupyter Notebook Screenshot 2](img/jnb2.PNG)   

Jupyter Notebook Screenshot 3:                   |  
:-----------------------------------------------:|
![Jupyter Notebook Screenshot 3](img/jnb3.PNG)   |  


Skillset 4 Calorie Percentage:                   |  
:-----------------------------------------------:|
![Skillset 4 Calorie Percentage](img/ss4.PNG)    |  

Skillset 5 Good Operator:                        |  Skillset 5 Bad Operator:                       |  Skillset 5 Cannot Divide by 0:
:-----------------------------------------------:|:-----------------------------------------------:|:-----------------------------------------------:
![Skillset 5 Good Operator](img/ss51.PNG)        |  ![Skillset 5 Bad Operator](img/ss52.PNG)      |   ![Skillset 5 Cannot Divide by 0](img/ss53.PNG)

Skillset 6 Loops:                                |  
:-----------------------------------------------:|
![Skillset 6 Loops](img/ss6.PNG)                 |  

#### Other Links:

*Click to return to the main page:*
[Home](https://bitbucket.org/Z_Hall19/lis4369/src/master/)
