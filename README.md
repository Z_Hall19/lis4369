> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions (Python)

## Zackary Hall

### Assignments:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio code
    - Create *a1_tip_calculator* application
    - Create *a1 tip calculator* Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create, run, and post screenshots of Payroll Calculator App
    - Run and post screenshots of Payroll Calculator App using Jupyter Notebook
    - Create, run, and post screenshots of completed skillsets
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create, run, and post screenshots of Painting Estimator App
        - Includes four functions:
            - get_requirements()
            - estimate_painting_cost()
            - print_painting_estimate()
            - print_painting_percentage()
    - Run and post screenshots of Painting Estimator App using Jupyter Notebook
    - Create, run, and post screenshots of completed skillsets 4, 5, and 6
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create, run, and post screenshots of Data Analysis 2 App
        - Includes three functions:
            - get_requirements(): Displays Program Requirements
            - data_analysis_2(): Displays results as per demo.py
            - main(): calls the two functions above
    - Run and post screenshots of Data Analysis 2 App using Jupyter Notebook
    - Create run and post screenshots of skillsets 10, 11, 12
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete Introduction_to_R_Setup_and_Tutorial
        - Save as learn_to_use_r.R
    - Code and run lis4369_a5.R
    - Screenshots of two plots from the Tutorial and Main Assignment
    - Create, run and post screenshots of skillsets 13, 14 and 15

### Projects:

*Course Work Links:*

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create, run, and post screenshots of Data Analysis App
        - Includes three functions:
            - get_requirements(): Displays Program Requirements
            - data_analysis_1(): Pulls data from a set time interval and outputs to screen
            - main(): calls the two functions above
    - Run and post screenshots of Data Analysis App using Jupyter Notebook
    - Create run and post screenshots of skillsets 7, 8, and 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Backwards-engineer the lis4369_p2_requirements.txt file
    - Save as lis4369_p2.R
    - Two 4-panel RStudio Screenshots executing lis4369_p2.R
    - Minimum of two plots
