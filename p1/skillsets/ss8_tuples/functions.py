#!/usr/bin/env python 3

def get_requirements():
    print("Python Tuples")
    print("\nProgram Requirements:\n"
         + "1. Tuples (Python data structure): *immutable* (cannot be changed!), ordered sequence of elements.\n"
         + "2. Tuples are immutable/unchangeable--that is, cannot inser, update, delete, \nNote: can reassign or delete and *entire* tuple--but, *not* individual items or slices.\n"
         + "3. Create tuple using parentheses (tuple): my_tuple1 = ('cherries', 'apples', 'bananas', 'oranges').\n"
         + "4. Create tuple (packing), that is,  *without* using parentheses (aka tuple \"packing\"): my_tuple2 = 1, 2, 'three, 'four\n"
         + "5. Python tuple (unpacking), that is, assign values from tuple to sequence of variables: fruit1, fruit2, fruit3, fruit4 = my_tuple1\n"
         + "6. Create a program that mirrors the following IPO (input/process/output) format.\n")


def using_tuples():

    my_tuple1 = ("cherries", "apples", "bananas", "oranges")

    my_tuple2 = 1, 2, "three", "four"


    print("\nOutput:")
    print("Print my_tuple1")
    print(my_tuple1)

    print()

    print("Print my_tuple2")
    print(my_tuple2)

    print()

    fruit1, fruit2, fruit3, fruit4 = my_tuple1
    print("Print my_tuple1 unpacking:")
    print(fruit1, fruit2, fruit3, fruit4)

    print()
    print("Print third element int my_tuple2:")
    print(my_tuple2[2])

    print()
    print("Print \"slice\" of my_tuple1 (second and third elements):")
    print(my_tuple1[1:3])

    print()
    print("Reassign my_tuple2 using parentheses.")
    my_tuple2 = (1,2,3,4)
    print("Print my_tuple2:")
    print(my_tuple2)

    print()
    print("Reassign my_tuple2 using \"packing\" method (no parentheses).")

    my_tuple2 = 5, 6, 7, 8

    print("Print my_tuple2:")
    print(my_tuple2)

    print()
    print("Print type of my_tuple1:" + str(type(my_tuple1)))

    print()
    print("Delete my_tuple1: \nNote: will generate error, if trying to print after, as it no longer exists.")

    del my_tuple1

    print()
    print("Attempt to print my_tuple1: ")
    print()
    print(my_tuple1)














def print_selection_structures(num1, num2, op):

    if op == "+":
        print(num1 + num2)

    elif op == "-":
        print(num1 - num2)
    elif op == "*":
        print(num1 * num2)
    elif op == "/":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 / num2)
    elif op == "//":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 // num2)
    elif op == "%":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 % num2)
    elif op == "**":

        print("Using ** operator: " + str(num1 ** num2))

        print("Using pow() function: " + str(pow(num1, num2)))
    else:
        print("Incorrect operator!")
