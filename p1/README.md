> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


## Zackary Hall

### Project 1 Requirements:

*Four Parts:*

1. Data Analysis App
2. Questions for Chapters 7 and 8
3. Bitbucket Repo Link
4. Skillsets 7, 8, and 9


#### README.md file should include the following items:

* Screenshot of Data Analysis App
    - get_requirements()
    - data_analysis_1()
* Link to p1.ipynb file: [a3_painting_estimator.ipynb](p1_data_analysis_1/p1_data_analysis_1.ipynb "P1 Jupyter Notebook")
* Screenshots of Jupyter Notebook
* Screenshots of Skillsets


#### Assignment Screenshots:
Data Analysis:                                   |  
:-----------------------------------------------:|
![Data Analysis](img/p1.PNG)                     |  

Jupyter Notebook Screenshot 1:                   |  Jupyter Notebook Screenshot 2:
:-----------------------------------------------:|:-----------------------------------------------:
![Jupyter Notebook Screenshot 1](img/jn1.PNG)    |  ![Jupyter Notebook Screenshot 2](img/jn2.PNG)   

Jupyter Notebook Screenshot 3:                   |  
:-----------------------------------------------:|
![Jupyter Notebook Screenshot 3](img/jn3.PNG)    |  
  

Skillset 7 Lists:                                |  Skillset 8 Tuples:                             |  Skillset 9 Sets:
:-----------------------------------------------:|:-----------------------------------------------:|:-----------------------------------------------:
![Skillset 7](img/ss7.PNG)                       |  ![Skillset 8](img/ss8.PNG)                     |   ![Skillset 9](img/ss9.PNG)



#### Other Links:

*Click to return to the main page:*
[Home](https://bitbucket.org/Z_Hall19/lis4369/src/master/)
